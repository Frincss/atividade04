package br.com.ucsal.entidades;

import br.com.ucsal.objetos.Carro;
import br.com.ucsal.objetos.Endereco;

public class Pessoa {
	/**
	 * Os propriet�rios possuem as seguintes caracter�sticas: cpf, nome, endere�o e
	 * o telefones. Lembre-se de que uma mesma pessoa pode ser propriet�ria de
	 * diversos ve�culos.
	 */
	private int lastPosi = 0, tam = 0;

	private Integer cpf;
	private String nome, telefone;
	private Endereco endereco;
	private Carro[] carros = new Carro[0];

	public void adicionarCarro(Carro carro) {
		aumentarQuantidadeCarros();
		carros[lastPosi] = carro;
		carros[lastPosi].setProprietario(this);
		lastPosi++;
	}

	private void aumentarQuantidadeCarros() {
		if (carros.length == 0) {
			carros = new Carro[1];
			tam++;
		} else {
			Carro[] c = carros;
			tam++;
			carros = new Carro[tam];
			for (int i = 0; i < c.length; i++) {
				carros[i] = c[i];
			}
		}
	}

	public void listarCarros() {
		if (lastPosi == 0)
			System.out.println("Essa pessoa n�o possui veiculos");
		else {
			for (int i = 0; i < lastPosi; i++) {
				System.out.println(carros[i] + "\n\n\n");
			}
		}
	}

	public Integer getCpf() {
		return cpf;
	}

	public void setCpf(Integer cpf) {
		this.cpf = cpf;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String toString() {
		return nome;
	}
}
